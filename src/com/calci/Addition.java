package com.calci;

/**
 * Created with IntelliJ IDEA.
 * User: Kanu
 * Date: 19/2/14
 */
public class Addition {
    Integer operand1, operand2;

    public Addition(Integer param1, Integer param2) {
        this.operand1 = param1;
        this.operand2 = param2;
    }

    public Integer add() {
        return operand1 + operand2;
    }
}
