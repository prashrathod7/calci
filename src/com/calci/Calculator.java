package com.calci;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Calculator {
	
	public static void main(String args[]) {
        Integer result;
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        try {
        	System.out.print("Enter First Parameter: ");
			int firstParameter = Integer.parseInt(br.readLine());
			System.out.print("Enter Second Parameter: ");
			int secondParameter = Integer.parseInt(br.readLine());
			
			System.out.println("Select Operation: ");
			System.out.println("1. Add ");
			System.out.println("2. Subtract ");
			System.out.println("3. Multiply ");
			System.out.println("4. Divide ");
			
			System.out.println("Enter Operation Ref Number: ");
			int operationRefNumber = Integer.parseInt(br.readLine());
			
			switch(operationRefNumber) {
				case 1:
					System.out.println("the operation is "+operationRefNumber);
					result = add(firstParameter, secondParameter);
                    System.out.println("result : " + result);
					break;
				case 2:
					System.out.println("the operation is "+operationRefNumber);
					//Subtract
					break;
				case 3:
					System.out.println("the operation is "+operationRefNumber);
					//Multiply
					break;
				case 4:
					System.out.println("the operation is "+operationRefNumber);
					//Divide
					break;
					default :
						System.out.println("You have Entered wrong operation Reference number. ");
			}
			
		} catch (IOException | NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

    private static Integer add(int firstParameter, int secondParameter) {
        Addition addition = new Addition(firstParameter, secondParameter);
        return addition.add();
    }
}
